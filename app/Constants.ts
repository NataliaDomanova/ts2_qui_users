export class Constants {
    public static LEXICAL_GRAMMAR_ID: string = 'LEXICAL_GRAMMAR_ID';
    public static READING_ID: string = 'READING_ID';
    public static LISTENING_ID: string = 'LISTENING_ID';
    public static SPEAKING_ID: string = 'SPEAKING_ID';
    public static  USER_ROLE: string = '0';
    public static TEACHER_ROLE: string = '1';
    public static ADMIN_ROLE: string = '2';

    // https://gentle-woodland-30415.herokuapp.com/api
    // http://localhost:8083/api

    public static BASE_URL: string = 'http://localhost:8083/api';
}