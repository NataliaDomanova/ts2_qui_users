import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router';
import {HeaderUser} from '../../user/ts-header-user/ts-header-user';
import {Footer} from "../../ts-footer/ts-footer";
import {Content} from "../../ts-content/ts-content";
import {TestForTeacher} from "../../../models/TeacherTestClass";
import {TsUserTestService} from "../../../models/ts-user-test.data.service";

@Component({
    selector: 'ts-teacher-check',
    templateUrl: './app/components/teacher/ts-teacher-check/ts-teacher-check.html',
    directives: [ROUTER_DIRECTIVES, HeaderUser, Footer, Content]
})

export class TsTeacherCheck {
    testSingleTon:TestForTeacher;
    counter:number;
    MAX_PART_COUNTER:number = 2;

    constructor(private router:Router,
                private testUserService:TsUserTestService) {
        this.testSingleTon = TestForTeacher.getInstance();
        this.counter = this.testSingleTon.getPartIsPassed();
    }

    checkListen() {
        if (!this.testSingleTon.getListeningConst()) {
            this.router.navigate(['/TsTeacherHome/TsTeacherListen']);
        }
    }

    checkSpeak() {
        if (!this.testSingleTon.getSpeakingConst()) {
            this.router.navigate(['/TsTeacherHome/TsTeacherSpeak']);
        }
    }

    postAllTest() {
        this.testSingleTon.setPartIsPassed(0);
        this.testUserService.postAllTestFromTeacher('2', this.testSingleTon.getTestId()).subscribe(data => {
            this.router.navigate(['/TsTeacherNotify']);
        });
    }
}