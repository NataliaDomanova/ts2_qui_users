/**
 * Created by Eva on 19.07.2016.
 */
import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router';
import { HeaderUser } from '../../user/ts-header-user/ts-header-user';
import {Footer} from "../../ts-footer/ts-footer";
import {Content} from "../../ts-content/ts-content";

@Component({
    selector: 'ts-teacher-home',
    templateUrl: './app/components/teacher/ts-teacher-home/ts-teacher-home.html',
    directives: [ROUTER_DIRECTIVES, HeaderUser, Footer, Content]
})

export class TsTeacherHome{
    
}