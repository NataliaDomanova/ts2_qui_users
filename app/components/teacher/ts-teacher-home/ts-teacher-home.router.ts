import {Authorization} from "../../authorization/authorization";
import {TsTeacherHome} from "./ts-teacher-home";
import {RouterConfig} from "@angular/router";
import {TsTeacherCheck} from "../ts-teacher-check/ts-teacher-check";
import {TsTeacherListen} from "../ts-teacher-listen/ts-teacher-listen";
import {TsTeacherSpeak} from "../ts-teacher-speak/ts-teacher-speak";
import {TsTeacherNotify} from "../ts-teacher-notify/ts-teacher-notify";
import {LoggedInGuard} from "../../authorization/logged-in.guard";


export const teacherRoutes:RouterConfig = [
    {path: '', component: Authorization},
    {path: 'TsTeacherHome', component: TsTeacherHome, canActivate: [LoggedInGuard]},
    {path: 'TsTeacherNotify', component: TsTeacherNotify, canActivate: [LoggedInGuard]},
    {
        path: 'TsTeacherHome', component: TsTeacherHome,
        children: [
            {path: 'TsTeacherCheck', component: TsTeacherCheck, canActivate: [LoggedInGuard]},
            {path: 'TsTeacherListen', component: TsTeacherListen, canActivate: [LoggedInGuard]},
            {path: 'TsTeacherSpeak', component: TsTeacherSpeak, canActivate: [LoggedInGuard]}
        ]
    }
];