import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router';
import {HeaderUser} from '../../user/ts-header-user/ts-header-user';
import {Footer} from "../../ts-footer/ts-footer";
import {Content} from "../../ts-content/ts-content";
import {TestForTeacher} from "../../../models/TeacherTestClass";
import {AnswerForTeacher} from "../../../models/answer/answersForTeacher";
import {TsUserTestService} from "../../../models/ts-user-test.data.service";
import {Constants} from "../../../Constants";
import {CheckedAnswers} from "../../../models/answer/checkedAnswers";

@Component({
    selector: 'ts-teacher-listen',
    templateUrl: './app/components/teacher/ts-teacher-listen/ts-teacher-listen.html',
    directives: [ROUTER_DIRECTIVES, HeaderUser, Footer, Content]
})

export class TsTeacherListen {
    testSingleTon:TestForTeacher;
    listeningTask:Array<AnswerForTeacher>;
    listeningAnswers:Array<CheckedAnswers>;

    constructor(private router:Router,
                private testService:TsUserTestService) {
        this.testSingleTon = TestForTeacher.getInstance();
        this.listeningTask = this.testSingleTon.getListeningTask();
        this.listeningAnswers = [];
        for(let i = 0; i < this.listeningTask.length; ++i){
            this.listeningAnswers[i] = new CheckedAnswers();
            this.listeningAnswers[i].answerId = this.listeningTask[i].answerId;
        }
    }

    postListening() {
        this.testSingleTon.setListeningConst(true);
        this.testSingleTon.incrementPartIsPassed();
        var topic = Constants.LISTENING_ID;
        this.testService.postCheckAnswers(this.listeningAnswers, topic, this.testSingleTon.getTestId())
            .subscribe(data => {
                    console.log(data);
                    this.router.navigate(['/TsTeacherHome/TsTeacherCheck']);
                },
                error => console.log(error));
    }
}
