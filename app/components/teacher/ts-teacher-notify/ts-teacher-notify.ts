import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router';
import {HeaderUser} from '../../user/ts-header-user/ts-header-user';
import {Footer} from "../../ts-footer/ts-footer";
import {Content} from "../../ts-content/ts-content";
import {TsUserTestService} from "../../../models/ts-user-test.data.service";
import {TestForTeacher} from "../../../models/TeacherTestClass";

@Component({
    selector: 'ts-teacher-notify',
    templateUrl: './app/components/teacher/ts-teacher-notify/ts-teacher-notify.html',
    directives: [ROUTER_DIRECTIVES, HeaderUser, Footer, Content]
})

export class TsTeacherNotify implements OnInit {
    testsForCheck:String[];
    testSingleTon:TestForTeacher;
    

    constructor(private router:Router,
                private testService:TsUserTestService) {
        this.testsForCheck = [];
        this.testSingleTon = TestForTeacher.getInstance();
    }

    getMessages() {
        this.testService.getAssignedTests().subscribe(messages => {
            this.testsForCheck = messages;
            console.log(this.testsForCheck);
        });
    }

    ngOnInit() {
        this.getMessages();
    }

    check(id:string) {
        this.testService.getTestForCheck(id)
            .subscribe(test => {this.testSingleTon.setTask(test);
            console.log(test);
                console.log(this.testSingleTon.getListeningTask());
                console.log(this.testSingleTon.getSpeakingTask());
                this.testSingleTon.setTestId(id);
            });
        this.router.navigate(['/TsTeacherHome/TsTeacherCheck']);
    }
}