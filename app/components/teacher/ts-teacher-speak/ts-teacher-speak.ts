import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router';
import {HeaderUser} from '../../user/ts-header-user/ts-header-user';
import {Footer} from "../../ts-footer/ts-footer";
import {Content} from "../../ts-content/ts-content";
import {TestForTeacher} from "../../../models/TeacherTestClass";
import {AnswerForTeacher} from "../../../models/answer/answersForTeacher";
import {CheckedAnswers} from "../../../models/answer/checkedAnswers";
import {Constants} from "../../../Constants";
import {TsUserTestService} from "../../../models/ts-user-test.data.service";

@Component({
    selector: 'ts-teacher-speak',
    templateUrl: './app/components/teacher/ts-teacher-speak/ts-teacher-speak.html',
    directives: [ROUTER_DIRECTIVES, HeaderUser, Footer, Content]
})

export class TsTeacherSpeak {
    testSingleTon:TestForTeacher;
    speakingTask:Array<AnswerForTeacher>;
    speakingAnswers:Array<CheckedAnswers>;
    url:string;
    isPlay:Array<boolean>;

    constructor(private router:Router,
                private testService:TsUserTestService) {
        this.testSingleTon = TestForTeacher.getInstance();
        this.speakingTask = this.testSingleTon.getSpeakingTask();
        this.speakingAnswers = [];
        this.isPlay = [];
        this.url = "http://localhost:8083//listening//questions//";
        for (let i = 0; i < this.speakingTask.length; ++i) {
            this.speakingAnswers[i] = new CheckedAnswers();
            this.speakingAnswers[i].answerId = this.speakingTask[i].answerId;
            console.log('spakingTask', this.speakingTask)
            this.isPlay[i] = false;
        }
    }

    playQuestion(index:number){
        event.preventDefault();
        this.isPlay[index] && document.getElementById(index.toString()).pause();
        !this.isPlay[index] && document.getElementById(index.toString()).play();
        this.isPlay[index] = !this.isPlay[index];
    }

    nextCheck() {
        this.testSingleTon.setSpeakingConst(true);
        this.testSingleTon.incrementPartIsPassed();
        var topic = Constants.SPEAKING_ID;
        this.testService.postCheckAnswers(this.speakingAnswers, topic, this.testSingleTon.getTestId())
            .subscribe(data => {
                    console.log(data);
                    this.router.navigate(['/TsTeacherHome/TsTeacherCheck']);
                },
                error => console.log(error));
    }
}
