/**
 * Created by Eva on 15.07.2016.
 */
import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router';
import { HeaderUser } from '../ts-header-user/ts-header-user';
import {Footer} from "../../ts-footer/ts-footer";
import {Content} from "../../ts-content/ts-content";

@Component({
    selector: 'ts-user-home',
    templateUrl: './app/components/user/ts-user-home/ts-user-home.html',
    directives: [ROUTER_DIRECTIVES, HeaderUser, Footer, Content]
})

export class TsUserHome {
}