import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, Router, ActivatedRoute} from '@angular/router';
import {HeaderUser} from '../ts-header-user/ts-header-user';
import {Footer} from "../../ts-footer/ts-footer";
import {Content} from "../../ts-content/ts-content";
import {TsUserTestService} from "../../../models/ts-user-test.data.service";
import {Statistics} from "../../../models/statistics/statistics";
import {StatisticsService} from "../../../models/statistics/statistics.service";

declare var d3:any;

@Component({
    selector: 'ts-user-statistics',
    templateUrl: './app/components/user/ts-user-statistics/ts-user-statistics.html',
    directives: [ROUTER_DIRECTIVES, HeaderUser, Footer, Content]
})

export class TsUserStatistics implements OnInit {
    testsList:any[];
    s:number = 0;
    test:any;
    sub:any;
    total:number;

    constructor(private testsService:StatisticsService,
                private route:ActivatedRoute,
                private router:Router) {
        this.test = {
            'part1': [],
            'part2': [],
            'part3': [],
            'part4': [],
            'marks': [0, 0, 0, 0],
            'isChecked': false,
            'conclusion':''
        };
        this.total = 0;
    }

    getTests(testId:number, userId:string) {
        this.testsService.getTestsList(userId).subscribe(tests => {
            this.testsList = tests;
            let test_Id = this.testsList[testId]._id;
            this.getStatistics(test_Id);
            (tests.length == 1) && (this.s = -1);
        });
    }

    getStatistics(id:string) {
        this.testsService.getTestById(id).subscribe(statistics => {
                this.test = new Statistics(statistics);
                this.total = this.test.marks.reduce(function(sum, current) {
                    return +sum + +current;
                }, 0);
                this.dashboard();
                console.log(this.test.conclusion);
                document.getElementById('language-assistant').innerHTML = this.test.conclusion;
            },
            err => console.error(err));
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            let id = params['id'];
            this.getTests(id, this.getUserId());
        });
    }

    section(num:number) {
        event.preventDefault();
        this.s = num;
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }


    getUserId() {
        var item = localStorage.getItem('user');
        if (item != null) {
            var user = item && JSON.parse(item);
            return user._id;
        }
    }

    onSelectTest(id:number) {
        event.preventDefault();
        this.router.navigate(['TsUserHome', id]);
    }

    dashboard() {

        var bubbleChart = new d3.svg.BubbleChart({
            supportResponsive: true,
            size: 600,
            innerRadius: 600 / 3.5,
            radiusMin: 50,
            data: {
                items: [
                    {text: "LGT", count: this.test.marks[0]},
                    {text: "reading", count: this.test.marks[1]},
                    {text: "listening", count: this.test.marks[2]},
                    {text: "speaking", count: this.test.marks[3]},
                    {text: "incorrect", count: "12"},
                ],
                eval: function (item) {
                    return item.count;
                },
                classed: function (item) {
                    return item.text.split(" ").join("");
                }
            },
            plugins: [
                {
                    name: "central-click",
                    options: {
                        style: {
                            "font-size": "12px",
                            "font-style": "italic",
                            "font-family": "Trajan Pro, sans-serif",
                            "text-anchor": "middle",
                            "fill": "white"
                        },
                        attr: {dy: "65px"}
                    }
                },
                {
                    name: "lines",
                    options: {
                        format: [
                            {
                                textField: "count",
                                classed: {count: true},
                                style: {
                                    "font-size": "28px",
                                    "font-family": "Trajan Pro, sans-serif",
                                    "text-anchor": "middle",
                                    fill: "white"
                                },
                                attr: {
                                    dy: "0px",
                                    x: function (d) {
                                        return d.cx;
                                    },
                                    y: function (d) {
                                        return d.cy;
                                    }
                                }
                            },
                            {
                                textField: "text",
                                classed: {text: true},
                                style: {
                                    "font-size": "14px",
                                    "font-family": "Trajan Pro, sans-serif",
                                    "text-anchor": "middle",
                                    fill: "white"
                                },
                                attr: {
                                    dy: "20px",
                                    x: function (d) {
                                        return d.cx;
                                    },
                                    y: function (d) {
                                        return d.cy;
                                    }
                                }
                            }
                        ],
                        centralFormat: [
                            {
                                style: {"font-size": "80px"},
                                attr: {}
                            },
                            {
                                style: {"font-size": "40px"},
                                attr: {dy: "40px"}
                            }
                        ]
                    }
                }]
        });
    }
}