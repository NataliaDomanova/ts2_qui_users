import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router';
import {HeaderUser} from '../ts-header-user/ts-header-user';
import {Footer} from "../../ts-footer/ts-footer";
import {Content} from "../../ts-content/ts-content";
import {LGQuestion} from "../../../models/question/question";
import {TsUserTestService} from "../../../models/ts-user-test.data.service";
import {Test} from "../../../models/UserTestClass";
import {CoolLocalStorage} from "angular2-cool-storage/index";
declare function BinaryClient({}):void;
declare var navigator:any;
declare function AudioContext():void;
@Component({
    selector: 'ts-user-speak',
    templateUrl: './app/components/user/ts-user-speak/ts-user-speak.html',
    directives: [ROUTER_DIRECTIVES, HeaderUser, Footer, Content]
})
export class TsUserSpeak {
    speakingQuestions:Array<LGQuestion>;
    testSingleTon:Test;
    localStorage:CoolLocalStorage;
    question:Object;
    counter:number;
    size:number;
    stopRecording: any;
    closeClient: any;
    port: number;
    isReadyToRecord: boolean;

    constructor(private router:Router,
                private TsUserTestService:TsUserTestService,
                localStorage: CoolLocalStorage) {

        this.speakingQuestions = [];
        this.testSingleTon = Test.getInstance();
        this.counter = 0;
        this.localStorage = localStorage;
        this.question = new LGQuestion();
        this.localStorage = localStorage;
        this.port = 9001;
        this.isReadyToRecord = false;
    }

    getTask() {
        this.TsUserTestService.getSpeakingQuestions(this.testSingleTon.getTestId())
            .subscribe(
                response => {
                    this.speakingQuestions = response.res;
                    this.question = this.speakingQuestions[this.counter];
                    this.size = this.speakingQuestions.length;
                    this.port = response.port;
                    this.isReadyToRecord = false;
                    this.recording();
                }
            );
    }

    ngOnInit() {
        this.getTask();
    }

    getUserId(){
        var item = localStorage.getItem('user');
        if (item != null) {
            var user = item && JSON.parse(item);
            return user._id;
        }
    }

    nextChapter() {     
        this.stopRecording();
        if (this.counter == this.size - 1) {
            this.testSingleTon.setSpeakingConst(true);
            this.testSingleTon.incrementPartIsPassed();
            this.closeClient();
            this.TsUserTestService.postAudioIsSend().subscribe(response => {
                this.router.navigate(['/TsUserHome/TsUserChoose']);
            });
            return;
        }
        
        this.isReadyToRecord = false;
        this.question = this.speakingQuestions[++this.counter];
    }


    recording(){
        (function(window) {
        
            var client  = new BinaryClient('ws://localhost:' + window.port);
            var endCb;
            var startCb;
            var recorder;

            window.stopRecording = function () {
                endCb();
                console.log('stop recorder')
            }

            window.closeClient = function () {
                client.close();
                console.log('client close');
            }
            
            client.on('open', function () {

                var recording = false;

                console.log('before navigator');

                if (!navigator.getUserMedia)
                    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia ||
                        navigator.mozGetUserMedia || navigator.msGetUserMedia;

                console.log('after 1 navigator');

                if (navigator.getUserMedia) {
                    navigator.getUserMedia({audio:true}, function(e) {
                        var res = success(e);
                        startCb = res.start;
                        endCb = res.end;

                    }, function(e) {
                        alert('Error capturing audio.');
                    });
                } else alert('getUserMedia not supported in this browser.');

                console.log('after 2 navigator');


                window.startRecording = function (id) {

                    console.log('id', id);

                    startCb().then(() =>{
                         if (!recording) {
                            window.Stream = client.createStream({
                                questionId: id,
                                testId: this.testSingleTon.getTestId(),
                                userId: this.getUserId()
                            });
                            console.log('start');
                            recording = true;
                            return;
                        }
                        recording = false;
                        console.log('end');
                        window.Stream.end();
                    });

                }

                function success(e) {
                    console.log('1');
                    var context = new AudioContext;
                    console.log('context ' + context);

                    var audioInput = context.createMediaStreamSource(e);

                    console.log('success');

                    var bufferSize = 2048;
                    recorder = context.createScriptProcessor(bufferSize, 1, 1);
                    console.log("recorder", recorder);

                    recorder.onaudioprocess = function (e) {
                        console.log('before recording', recording);
                        if (!recording) return;
                        console.log('recording');
                        var leftChannel = e.inputBuffer.getChannelData(0);
                        window.Stream.write(convertoFloat32ToInt16(leftChannel));
                    }

                    return {
                        end: function () {
                            audioInput.disconnect(recorder)
                            recorder.disconnect(context.destination);
                        },
                        start: function () {
                            let promise = new Promise((resolve, reject) => {

                              setTimeout(() => {
                                    window.isReadyToRecord = true;
                                    console.log("isReadyToRecord");
                                    audioInput.connect(recorder)
                                    recorder.connect(context.destination);
                                    resolve();
                                
                              }, 1000);

                            });
                            return promise;
                        }
                    }
                }

                function convertoFloat32ToInt16(buffer) {
                    var length = buffer.length;
                    var buf = new Int16Array(length)

                    while (length--) {
                        buf[length] = buffer[length] * 0xFFFF;    //convert to 16 bit
                    }
                    return buf.buffer
                }
            });
        })(this);
    }
}
