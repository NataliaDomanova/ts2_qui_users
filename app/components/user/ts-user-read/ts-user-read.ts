import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router';
import {HeaderUser} from '../ts-header-user/ts-header-user';
import {Footer} from "../../ts-footer/ts-footer";
import {Content} from "../../ts-content/ts-content";
import {ReadingTask} from "../../../models/question/ts-user-test.modelReadingTask";
import {TsUserTestService} from "../../../models/ts-user-test.data.service";
import {Test} from "../../../models/UserTestClass";
@Component({
    selector: 'ts-user-speak',
    templateUrl: './app/components/user/ts-user-read/ts-user-read.html',
    directives: [ROUTER_DIRECTIVES, HeaderUser, Footer, Content]
})

export class TsUserRead implements OnInit{
    readingTask: ReadingTask;
    testSingleTon: Test;

    constructor(private router: Router, 
                private testService: TsUserTestService) {
        this.readingTask = new ReadingTask();
        this.testSingleTon = Test.getInstance();
    }

    getTask(){
        this.testService.getReadingTask(this.testSingleTon.getTestId())
            .subscribe(
                readingTask => {
                    this.readingTask = readingTask;
                    this.testSingleTon.setReadingTask(readingTask);
                }
            );
    }

    ngOnInit(){
        this.getTask();
    }

    readTest() {
        this.testSingleTon.setReadingConst(true);
        this.router.navigate(['/TsUserHome/TsUserReadTest']);
    }

}