import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, Router, Event, NavigationEnd} from '@angular/router';
import {Test} from "../../../models/UserTestClass";
import {UserService} from "../../authorization/user.service";

@Component({
    selector: 'ts-header-user',
    templateUrl: './app/components/user/ts-header-user/ts-header-user.html',
    directives: [ROUTER_DIRECTIVES]
})

export class HeaderUser {
    showCountdown:boolean = false;
    isStarted:boolean = false;
    user:any;
    parts:Array<any>;
    time:any;
    testSingleTon:Test;
    duration:any;
    startDate:any;

    constructor(public router:Router,
                private userService:UserService) {
        this.user = {};
        this.time = 0;
        this.parts = [];
        this.getUser();
        this.testSingleTon = Test.getInstance();
        this.duration = this.testSingleTon.getDuration();
        this.startDate = new Date();
        this.startDate.setSeconds(this.startDate.getSeconds() + this.duration * 60 + 1);
        this.startTime.call(this);
    }
    
    getUser(){
        var item = localStorage.getItem('user');
        if (item != null) {
            var user = item && JSON.parse(item);
            this.user.firstName = user.firstName;
            this.user.lastName = user.lastName;
            this.user.mail = user.mail;
            this.user.photo = user.photo;
        }
    }

    logout(){
        event.preventDefault();
        this.userService.logout();
        this.router.navigate(['']);
    }

    startTime() {
        this.router.events.subscribe((event:Event) => {
            if (event instanceof NavigationEnd) {
                if ((event.url == '/TsUserHome/TsUserTest') || (event.url == '/TsUserHome/TsUserChoose')
                    || (event.url == '/TsUserHome/TsUserListening') || (event.url == '/TsUserHome/TsUserRead')
                    || (event.url == '/TsUserHome/TsUserSpeak') || (event.url == '/TsUserHome/TsUserReadTest')) {
                    this.showCountdown = true;
                    if (!this.isStarted) {
                        var intervalID = setInterval(() => this.tick(), 1000);
                    }
                    this.isStarted = true;
                } else if (intervalID) {
                    clearInterval(intervalID);
                }
            }
        })
    }

    tick() {
        var one_second = 1000,
            one_minute = one_second * 60,
            one_hour = one_minute * 60;


        var now = +new Date(),
            timer = +this.startDate - now;

        this.parts[0] = '' + Math.floor(timer / one_minute);
        this.parts[1] = '' + Math.floor((timer % one_minute) / one_second);

        this.parts[0] = (this.parts[0].length == 1) ? '0' + this.parts[0] : this.parts[0];
        this.parts[1] = (this.parts[1].length == 1) ? '0' + this.parts[1] : this.parts[1];

        this.time = this.parts.join(':');
    }
}


