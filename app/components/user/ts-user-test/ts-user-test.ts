import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router';
import {HeaderUser} from '../ts-header-user/ts-header-user';
import {Footer} from "../../ts-footer/ts-footer";
import {Content} from "../../ts-content/ts-content";
import {LGQuestion} from "../../../models/question/question";
import {TsUserTestService} from "../../../models/ts-user-test.data.service";
import {Answer} from "../../../models/answer/answer";
import {FORM_DIRECTIVES} from '@angular/common';
import {Constants} from "../../../Constants";
import {Test} from "../../../models/UserTestClass";

@Component({
    selector: 'ts-user-test',
    templateUrl: './app/components/user/ts-user-test/ts-user-test.html',
    directives: [ROUTER_DIRECTIVES, HeaderUser, Footer, Content, FORM_DIRECTIVES]
})

export class TsUserTest implements OnInit {
    questionList:Array<LGQuestion>;
    testSingleTon:Test;
    userAnswers:Array<Answer>;
    answer:Answer;
    question:Object;
    counter:number;
    size:number;

    constructor(private router:Router,
                private testService:TsUserTestService) {
        this.testSingleTon = Test.getInstance();
        this.userAnswers = [];
        this.questionList = [];
        this.answer = new Answer();
        this.question = new LGQuestion();
        this.counter = 0;
        this.size = 0;
    }


    getQuestions() {
        this.testService.getLGTest()
            .subscribe(
                questions => {
                    this.questionList = questions;
                    this.question = this.questionList[this.counter];
                    this.size = this.questionList.length;
                }
            );
    }

    ngOnInit() {
        this.getQuestions();
    }

    nextChapter(questionId:string) {
        this.answer.questionId = questionId;
        var safe = new Answer();
        safe.answer = this.answer.answer;
        safe.questionId = this.answer.questionId;
        this.userAnswers.push(safe);
        if (this.counter == this.size - 1) {
            var topic = Constants.LEXICAL_GRAMMAR_ID;
            this.testService.postAnswers(this.userAnswers, topic, this.testSingleTon.getTestId())
                .subscribe(
                    data => {
                        this.testSingleTon.incrementPartIsPassed();
                        this.router.navigate(['/TsUserHome/TsUserChoose']);
                    },
                    error => console.log(error)
                );
            return;
        }
        this.question = this.questionList[++this.counter];
        this.answer = new Answer();
    }
}

