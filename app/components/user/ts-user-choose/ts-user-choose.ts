import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router';
import {HeaderUser} from '../ts-header-user/ts-header-user';
import {Footer} from "../../ts-footer/ts-footer";
import {Content} from "../../ts-content/ts-content";
import {Test} from "../../../models/UserTestClass";
import {TsUserTestService} from "../../../models/ts-user-test.data.service";
@Component({
    selector: 'ts-user-choose',
    templateUrl: './app/components/user/ts-user-choose/ts-user-choose.html',
    directives: [ROUTER_DIRECTIVES, HeaderUser, Footer, Content]
})

export class TsUserChoose {
    testSingleTon:Test;
    counter:number;
    MAX_PART_COUNTER:number = 4;

    constructor(private router:Router,
                private testUserService:TsUserTestService) {
        this.testSingleTon = Test.getInstance();
        this.counter = this.testSingleTon.getPartIsPassed();
        console.log('counter=' + this.counter);
    }

    speak() {
        if(!this.testSingleTon.getSpeakingConst()){
            this.router.navigate(['/TsUserHome/TsUserSpeak']);
        }
    }

    reading() {
        if(!this.testSingleTon.getReadingConst()) {
            this.router.navigate(['/TsUserHome/TsUserRead']);
        }
    }

    listening() {
        if(!this.testSingleTon.getListeningConst()) {
            this.router.navigate(['/TsUserHome/TsUserListening']);
        }
    }

    postAllTest() {
        this.testSingleTon.setPartIsPassed(0);
        this.testUserService.postAllTest('1', this.testSingleTon.getTestId()).subscribe(data => {
            console.log('data ', data);
            this.counter = 0;
            this.testSingleTon.setListeningConst(false);
            this.testSingleTon.setReadingConst(false);
            this.testSingleTon.setLGTConst(false);
            this.testSingleTon.setSpeakingConst(false);
            this.router.navigate(['/TsUserHome1Test']);
        });
    }
}
