import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router';
import {HeaderUser} from '../ts-header-user/ts-header-user';
import {Footer} from "../../ts-footer/ts-footer";
import {Content} from "../../ts-content/ts-content";
import {ReadingTask} from "../../../models/question/ts-user-test.modelReadingTask";
import {TsUserTestService} from "../../../models/ts-user-test.data.service";
import {Test} from "../../../models/UserTestClass";
@Component({
    selector: 'ts-user-listening',
    templateUrl: './app/components/user/ts-user-listening/ts-user-listening.html',
    directives: [ROUTER_DIRECTIVES, HeaderUser, Footer, Content]
})

export class TsUserListening {

    listeningTask: ReadingTask;
    pathAudio: string;
    testSingleTon: Test;
    isPlay:boolean = false;

    constructor(private router:Router,
                private TsUserTestService:TsUserTestService) {
        this.testSingleTon = Test.getInstance();
        this.listeningTask = new ReadingTask();
    }

    getTask() {
        this.TsUserTestService.getListeningTask(this.testSingleTon.getTestId())
            .subscribe(
                listeningTask => {
                    this.listeningTask = listeningTask;
                    this.pathAudio = "http://localhost:8083//listening//questions//"+ listeningTask.text;
                    console.log(this.pathAudio);
                    this.testSingleTon.setReadingTask(listeningTask);
                    console.log(this.testSingleTon.getReadingTask());
                    console.log(this.listeningTask);
                }
            );

    }

    ngOnInit() {
        this.getTask();
    }
    
    playQuestion(){
        event.preventDefault();
        // this.isPlay && document.getElementById('player').pause();
        // !this.isPlay && document.getElementById('player').play();
        this.isPlay = !this.isPlay;
    }

    listen() {
        this.testSingleTon.setListeningConst(true);
        this.router.navigate(['/TsUserHome/TsUserListeningTest']);
    }
    
}