export class User{
    id: string;
    firstName: string;
    lastName: string;
    role: number;
    mail: string;
    photo: string;
}