import {RouterConfig} from '@angular/router';

import {TsUserHome1Test} from '../ts-user-home-1test/ts-user-home-1test';
import {Authorization} from '../../authorization/authorization';
import {TsUserTest} from "../ts-user-test/ts-user-test";
import {TsUserChoose} from "../ts-user-choose/ts-user-choose";
import {TsUserHome} from "../ts-user-home/ts-user-home";
import {TsUserSpeak} from "../ts-user-speak/ts-user-speak";
import {TsUserRead} from "../ts-user-read/ts-user-read";
import {TsUserListening} from "../ts-user-listening/ts-user-listening";
import {TsUserReadTest} from "../ts-user-read-test/ts-user-read-test";
import {TsUserStatistics} from "../ts-user-statistics/ts-user-statistics";
import {TsUserListeningTest} from "../ts-user-listening-test/ts-user-listening-test";
import {LoggedInGuard} from "../../authorization/logged-in.guard";

//noinspection TypeScriptValidateTypes
export const userRoutes:RouterConfig = [
    {path: '', component: Authorization},
    {path: 'TsUserHome1Test', component: TsUserHome1Test, canActivate: [LoggedInGuard]},
    {path: 'TsUserHome', component: TsUserHome, canActivate: [LoggedInGuard]},
    {
        path: 'TsUserHome', component: TsUserHome,
        children: [
            {path: 'TsUserTest', component: TsUserTest, canActivate: [LoggedInGuard]},
            {path: 'TsUserChoose', component: TsUserChoose, canActivate: [LoggedInGuard]},
            {path: 'TsUserSpeak', component: TsUserSpeak, canActivate: [LoggedInGuard]},
            {path: 'TsUserRead', component: TsUserRead, canActivate: [LoggedInGuard]},
            {path: 'TsUserListening', component: TsUserListening, canActivate: [LoggedInGuard]},
            {path: 'TsUserReadTest', component: TsUserReadTest, canActivate: [LoggedInGuard]},
            {path: 'TsUserStatistics', component: TsUserStatistics, canActivate: [LoggedInGuard]},
            {path: 'TsUserListeningTest', component: TsUserListeningTest, canActivate: [LoggedInGuard]},
            {path: ':id', component: TsUserStatistics, canActivate: [LoggedInGuard] }
        ]
    }
];