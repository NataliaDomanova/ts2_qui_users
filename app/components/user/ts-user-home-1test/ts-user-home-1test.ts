import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router';
import {HeaderUser} from '../ts-header-user/ts-header-user';
import {Footer} from "../../ts-footer/ts-footer";
import {Content} from "../../ts-content/ts-content";
import {TsUserTestService} from "../../../models/ts-user-test.data.service";
import {Test} from "../../../models/UserTestClass";
import {User} from "./user.model";

@Component({
    selector: 'ts-user-home-1test',
    templateUrl: './app/components/user/ts-user-home-1test/ts-user-home-1test.html',
    directives: [ROUTER_DIRECTIVES, HeaderUser, Footer, Content]
})

export class TsUserHome1Test implements OnInit {
    userPassedTests:any;
    testSingleTon:Test;
    leftDays:any;
    leftHours:any;
    leftMinutes:any;
    startDate:any;
    finishDate:any;
    isActiveTest:boolean = false;

    constructor(private router:Router,
                private userService:TsUserTestService) {
        this.userPassedTests = {};
        this.userPassedTests.id = '';
        this.testSingleTon = Test.getInstance();
        this.leftDays = 0;
        this.leftHours = 0;
        this.leftMinutes = 0;
        this.startDate = 0;
        this.finishDate = 0;
        this.getAssignedTests.call(this);
    }

    getAssignedTests() {
        this.userService.getAssignedTests()
            .subscribe(
                userPassedTest => {
                    this.userPassedTests = userPassedTest;
                    console.log(this.userPassedTests );
                    this.testSingleTon.setTestId(this.userPassedTests.id);
                    this.testSingleTon.setDuration(this.userPassedTests.duration);
                    this.startDate = +this.userPassedTests.startTime;
                    this.finishDate = +this.userPassedTests.finishTime;
                    if (this.startDate <= new Date()) {
                        this.isActiveTest = true;
                        var intervalID = setInterval(() => this.tick(), 1000);
                    }
                    if (this.finishDate < new Date()) {
                        this.isActiveTest = false;
                        clearInterval(intervalID);
                    }
                },
                error => console.log(error));
    }

    ngOnInit() {
        this.getAssignedTests();
    }

    startTest() {
        this.router.navigate(['/TsUserHome/TsUserTest']);
    }

    requestTest() {
        this.userService.requestTest('0')
            .subscribe(date => console.log(date),
                error => console.log(error));
    }

    statistics() {
        let id = 0;
        this.router.navigate(['/TsUserHome', id]);
    }

    tick() {
        var one_second = 1000,
            one_minute = one_second * 60,
            one_hour = one_minute * 60,
            one_day = one_hour * 24;

        var now = +new Date(),
            timer = +this.finishDate - now;
        this.leftDays = '' + Math.floor(timer / one_day);
        this.leftHours = '' + Math.floor(( timer % one_day) / one_hour);
        this.leftMinutes = '' + Math.floor((( timer % one_day) % one_hour) / one_minute);
    }
}


