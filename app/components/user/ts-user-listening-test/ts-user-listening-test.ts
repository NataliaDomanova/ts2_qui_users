import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router';
import {HeaderUser} from '../ts-header-user/ts-header-user';
import {Footer} from "../../ts-footer/ts-footer";
import {LGQuestion} from "../../../models/question/question";
import {Content} from "../../ts-content/ts-content";
import {TsUserTestService} from "../../../models/ts-user-test.data.service";
import {Answer} from "../../../models/answer/answer";
import {Constants} from "../../../Constants";
import {Test} from "../../../models/UserTestClass";
@Component({
    selector: 'ts-user-read-test',
    templateUrl: './app/components/user/ts-user-listening-test/ts-user-listening-test.html',
    directives: [ROUTER_DIRECTIVES, HeaderUser, Footer, Content]
})

export class TsUserListeningTest {
    listeningQuestions:Array<LGQuestion>;
    userAnswers:Array<Answer>;
    answer:Answer;
    question:Object;
    counter:number;
    testSingleTon = Test.getInstance();
    size:number;

    constructor(private router:Router,
                private testService:TsUserTestService) {
        this.userAnswers = [];
        this.answer = new Answer();
        this.question = new LGQuestion();
        this.counter = 0;
        this.size = 0;
        this.listeningQuestions = [];
    }

    getQuestions() {
        this.listeningQuestions = this.testSingleTon.getReadingTask().questions;
        this.question = this.listeningQuestions[this.counter];
        this.size = this.listeningQuestions.length;
    }

    ngOnInit() {
        this.getQuestions();
    }

    nextChapter(questionId:string) {
        this.answer.questionId = questionId;
        var safe = new Answer();
        safe.answer = this.answer.answer;
        safe.questionId = this.answer.questionId;
        this.userAnswers.push(safe);
        this.answer.answer = null;
        if (this.counter == this.size - 1) {
            var topic = Constants.LISTENING_ID;
            this.testService.postAnswers(this.userAnswers, topic, this.testSingleTon.getTestId())
                .subscribe(
                    data => {
                        this.testSingleTon.incrementPartIsPassed();
                        this.router.navigate(['/TsUserHome/TsUserChoose']);
                    },
                    error => console.log(error.json().message)
                );
            return;
        }
        this.question = this.listeningQuestions[++this.counter];

    }
}