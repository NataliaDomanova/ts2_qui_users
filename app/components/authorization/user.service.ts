import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Constants} from "../../Constants";

@Injectable()
export class UserService{
    private loggedIn = false;

    constructor(private http: Http) {
        this.getUserToken();
    }

    getUserToken(){
        var item = localStorage.getItem('user');
        if (item != null) {
            var user = item && JSON.parse(item);
            this.loggedIn = !!user.token;
        }
    }

    login(user) {
        let headers = new Headers({'Content-Type': 'application/json'});
        let body = JSON.stringify({user});
        console.log(body);

        let url = Constants.BASE_URL + '/login';

        return this.http.post(url, body, {headers: headers})
        .map(res => res.json())
                .map((res) => {
                    if (res) {
                        console.log('res in login' + res);
                        this.loggedIn = true;
                        localStorage.setItem('user', JSON.stringify(res.user));

                    }
                    console.log(res);
                    return res;
                });
        }

    logout() {
        localStorage.removeItem('user');
        this.loggedIn = false;
    }

    isLoggedIn() {
        return this.loggedIn;
    }
}