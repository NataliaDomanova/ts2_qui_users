import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {Footer} from "../ts-footer/ts-footer";

import {UserService} from './user.service';
import {Constants} from "../../Constants";
import {TsUserTestService} from "../../models/ts-user-test.data.service";

@Component({
    selector: 'authorization',
    templateUrl: './app/components/authorization/authorization.html',
    directives: [Footer]
})

export class Authorization {
    errMessage:string;

    constructor(private userService:UserService,
                private router:Router) {
    }

    onSubmit(user) {
        this.userService.login(user).subscribe((result) => {
            if (result) {
                if (result.user.role == Constants.USER_ROLE) {
                    this.router.navigate(['/TsUserHome1Test']);
                }
                else if (result.user.role == Constants.TEACHER_ROLE) {
                    this.router.navigate(['/TsTeacherNotify']);
                }
                else {
                    this.errMessage = 'Incorrect login or password';
                }
            } else this.errMessage = 'Incorrect login or password';
        });
    }
}
