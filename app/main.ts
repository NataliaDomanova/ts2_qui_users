import { bootstrap }    from '@angular/platform-browser-dynamic';
import { APP_ROUTER_PROVIDERS } from './app.routes';
import { AppComponent } from './app.component';
import { LocationStrategy,
    HashLocationStrategy } from '@angular/common';
import {COOL_STORAGE_PROVIDERS} from "angular2-cool-storage";
import {UserService} from "./components/authorization/user.service";
import {LoggedInGuard} from "./components/authorization/logged-in.guard";
import {HTTP_PROVIDERS} from "@angular/http";
import {Test} from "./models/UserTestClass";
import {TsUserTestService} from "./models/ts-user-test.data.service";
import {StatisticsService} from "./models/statistics/statistics.service";

//noinspection TypeScriptValidateTypes
bootstrap(AppComponent, [COOL_STORAGE_PROVIDERS, UserService, LoggedInGuard, HTTP_PROVIDERS, Test,
    APP_ROUTER_PROVIDERS, TsUserTestService, StatisticsService,
    { provide: LocationStrategy, useClass: HashLocationStrategy}
])
    .catch(err => console.error(err));
  