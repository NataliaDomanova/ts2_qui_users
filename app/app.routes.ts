import {provideRouter, RouterConfig} from '@angular/router';
import { userRoutes } from './components/user/ts-user-home-1test/ts-user-home-1test.router';

import {Authorization} from './components/authorization/authorization';
import {teacherRoutes} from "./components/teacher/ts-teacher-home/ts-teacher-home.router";



//noinspection TypeScriptValidateTypes
export const routes:RouterConfig = [
    {path: '', component: Authorization},
    ...teacherRoutes,
    ...userRoutes
];

export const APP_ROUTER_PROVIDERS = [
    provideRouter(routes)
];
