import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import {TsUserTestService} from "./models/ts-user-test.data.service";
import {HTTP_PROVIDERS} from '@angular/http'

@Component({
    selector: 'my-app',
    template: `
                <div class="outer-outlet">
                  <router-outlet></router-outlet>
                  </div>`,
    directives: [ROUTER_DIRECTIVES]
})
export class AppComponent {
}
