import {Injectable} from '@angular/core';
import {ReadingTask} from "./question/ts-user-test.modelReadingTask";
import {TestModel} from "./test/testModel";

@Injectable()
export class Test {

    static _instance:Test = new Test();
    static readingTask:ReadingTask;
    static test:TestModel;
    static LGT:boolean = false;
    static SPEAKING:boolean = false;
    static LISTENING:boolean = false;
    static READING:boolean = false;
    static partsIsPassed:number = 0;
    static duration:any;
    static id:string = '';
    static port: number;

    constructor() {
        if (Test._instance) {
            throw new Error("Error: Instantiation failed: Use SingletonDemo.getInstance() instead of new.");
        }
        Test._instance = this;
    }

    public static getInstance():Test {
        return Test._instance;
    }

    setPort(port: number) {
        Test.port = port;
    }

    getPort() {
        return Test.port;
    }

    getDuration() {
        return Test.duration;
    }
    
    setDuration(duration:any){
        Test.duration = duration;
    }
    
    getTestId() {
        return Test.id;
    }

    setTestId(id:string) {
        Test.id = id;
    }

    setReadingTask(readingTask:any) {
        Test.readingTask = readingTask;
    }

    getLGTConst():boolean {
        return Test.LGT;
    }


    setLGTConst(passed:boolean) {
        Test.LGT = passed;
    }

    getListeningConst():boolean {
        return Test.LISTENING;
    }

    setListeningConst(passed:boolean) {
        Test.LISTENING = passed;
    }

    getSpeakingConst():boolean {
        return Test.SPEAKING;
    }

    setSpeakingConst(passed:boolean) {
        Test.SPEAKING = passed;
    }

    getReadingConst():boolean {
        return Test.READING;
    }

    setReadingConst(passed:boolean) {
        Test.READING = passed;
    }

    getPartIsPassed() {
        return Test.partsIsPassed;
    }

    incrementPartIsPassed() {
        Test.partsIsPassed++;
    }

    setPartIsPassed(counter:number) {
        Test.partsIsPassed = counter;
    }

    getReadingTask() {
        return Test.readingTask;
    }

}