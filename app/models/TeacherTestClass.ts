import {Injectable} from '@angular/core';
import {AnswerForTeacher} from "./answer/answersForTeacher";

@Injectable()
export class TestForTeacher {

    static _instance:TestForTeacher = new TestForTeacher();
    static partsIsPassed:number = 0;
    static id:string = '';
    static listening:Array<AnswerForTeacher> = [];
    static speaking:Array<AnswerForTeacher> = [];
    static SPEAKING:boolean = false;
    static LISTENING:boolean = false;

    constructor() {
        if (TestForTeacher._instance) {
            throw new Error("Error: Instantiation failed: Use SingletonDemo.getInstance() instead of new.");
        }
        TestForTeacher._instance = this;
    }

    public static getInstance():TestForTeacher {
        return TestForTeacher._instance;
    }

    getTestId() {
        return TestForTeacher.id;
    }

    setTestId(id:string) {
        TestForTeacher.id = id;
    }

    getListeningTask() {
        return TestForTeacher.listening;
    }

    setListeningTask(listeningTask:any) {
        TestForTeacher.listening = listeningTask;
    }

    getSpeakingTask() {
        return TestForTeacher.speaking;
    }

    setSpeakingTask(speakingTask:any) {
        TestForTeacher.speaking = speakingTask;
    }

    incrementPartIsPassed() {
        TestForTeacher.partsIsPassed++;
    }

    setTask(obj){
        console.log("obj", obj);
        TestForTeacher.listening = obj.listening;
        TestForTeacher.speaking = obj.speaking;
    }

    getListeningConst():boolean {
        return TestForTeacher.LISTENING;
    }

    setListeningConst(passed:boolean) {
        TestForTeacher.LISTENING = passed;
    }

    getSpeakingConst():boolean {
        return TestForTeacher.SPEAKING;
    }

    setSpeakingConst(passed:boolean) {
        TestForTeacher.SPEAKING = passed;
    }

    getPartIsPassed() {
        return TestForTeacher.partsIsPassed;
    }

    setPartIsPassed(counter:number) {
        TestForTeacher.partsIsPassed = counter;
    }
}
