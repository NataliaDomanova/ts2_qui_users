import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import {LGQuestion} from "./question/question";
import {ReadingTask} from "./question/ts-user-test.modelReadingTask";
import {Answer} from "./answer/answer";
import {Constants} from "../Constants";
import {AnswerForTeacher} from "./answer/answersForTeacher";
import {CheckedAnswers} from "./answer/checkedAnswers";
import {User} from "../components/user/ts-user-home-1test/user.model";

@Injectable()
export class TsUserTestService {
    authToken:string;
    auth_id:string;

    constructor(private http:Http) {

        var item = localStorage.getItem('user');
        if (item != null) {
            var user = item && JSON.parse(item);
            this.authToken = user.token;
            this.auth_id = user._id;
        }
    }

    postAudioIsSend(): Observable<any> {
        let headers = new Headers({'Authorization': this.authToken, 'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        var url = Constants.BASE_URL + '/userAnswers/audioIsSend';
        return this.http.get(url, options);
    }

    getUserById():Observable<User>{
        let headers = new Headers({'Authorization': this.authToken, 'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        var url = Constants.BASE_URL + '/users/' + this.auth_id;
        return this.http.get(url, options)
            .map(response => response.json());
    }

    getLGTest():Observable<LGQuestion[]> {
        let headers = new Headers({'Authorization': this.authToken, 'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        var url = Constants.BASE_URL + '/tests/' + this.auth_id + '/startTest';
        return this.http.get(url, options)
            .map(response => response.json());
    }


    getReadingTask(id:string):Observable<ReadingTask> {
        let headers = new Headers({'Authorization': this.authToken, 'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        var url = Constants.BASE_URL + '/tests/' + this.auth_id + '/getReadingTest';
        return this.http.get(url, options)
            .map(response => response.json());
    }

    getListeningTask(id:string):Observable<ReadingTask> {
        let headers = new Headers({'Authorization': this.authToken, 'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        var url = Constants.BASE_URL + '/tests/' + this.auth_id + '/getListeningTest';
        return this.http.get(url, options)
            .map(response => response.json());
    }

    getSpeakingQuestions(id:string):Observable<any> {
        let headers = new Headers({'Authorization': this.authToken, 'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        var url = Constants.BASE_URL + '/tests/' + this.auth_id + '/getSpeakingTest';
        return this.http.get(url, options)
            .map(response => response.json());
    }

    postAnswers(answers:Array<Answer>, topic:Object, id:string):Observable<any> {
        let body = JSON.stringify({answers, topic});
        let headers = new Headers({'Authorization': this.authToken, 'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        console.log(body);
        console.log(options);
        let url = Constants.BASE_URL + '/userAnswers/' + id;
        return this.http.post(url, body, options);
    }

    postAudio(answer:any, id:string):Observable<any> {
        let body = JSON.stringify({answer});
        let headers = new Headers({'Authorization': this.authToken, 'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        console.log(body);
        let url = Constants.BASE_URL + '/userAnswers/' + '/sendAudio';
        return this.http.post(url, body, options)
            .map(response => response.json());
    }

    postAllTest(event:string, id:string):Observable<any> {
        let auth_id = this.auth_id;
        let body = JSON.stringify({notification: {auth_id, 'event': event, 'date': new Date()}});
        let headers = new Headers({'Authorization': this.authToken, 'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        console.log(body);
        let url = Constants.BASE_URL + '/tests/' + id + '/isPassed';
        return this.http.post(url, body, options);
    }

    postAllTestFromTeacher(event:string, id:string):Observable<any> {
        let reviewerId = this.auth_id;
        let body = JSON.stringify({notification: {reviewerId, 'event': event, 'date': new Date()}});
        let headers = new Headers({'Authorization': this.authToken, 'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let url = Constants.BASE_URL + '/tests/' + id + '/isChecked';
        return this.http.post(url, body, options);
    }

    getAssignedTests():Observable<any> {
        console.log('id ', this.auth_id);
        console.log(this.authToken);
        let headers = new Headers({'Authorization': this.authToken});
        let options = new RequestOptions({headers: headers});
        console.log('option ', options);
        var url = Constants.BASE_URL + '/tests/assign/' + this.auth_id;
        console.log(this.auth_id);
        return this.http.get(url, options)
            .map(response => response.json());
    }
    
    getTestForCheck(id:string):Observable<AnswerForTeacher>{
        console.log(id);
        let headers = new Headers({'Authorization': this.authToken});
        let options = new RequestOptions({headers: headers});
        var url = Constants.BASE_URL + '/userAnswers/' + id;
        return this.http.get(url, options)
            .map(response => response.json());
    }

    // getTestById(id:number):Observable<Test> {
    //     let headers = new Headers({'Authorization': this.authToken});
    //     let options = new RequestOptions({headers: headers});
    //     var url = Constants.BASE_URL + '/userAnswers/' + '/statistics/' + id;
    //     return this.http.get(url, options)
    //         .map(response => response.json());
    // }

    requestTest(event:string): Observable<any>{
        let auth_id = this.auth_id;
        let body = JSON.stringify({notification: {auth_id, 'event': event, 'date': new Date()}});
        console.log(body);
        let headers = new Headers({'Authorization': this.authToken, 'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let url = Constants.BASE_URL + '/tests/isRequested';
        console.log(body);
        return this.http.post(url, body, options);
    }

    postCheckAnswers(answers:Array<CheckedAnswers>, topic:Object, id:string):Observable<any> {
        let body = JSON.stringify({answers, topic});
        let headers = new Headers({'Authorization': this.authToken, 'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        console.log(body);
        let url = Constants.BASE_URL + '/userAnswers/checked/' + id;
        return this.http.post(url, body, options)
            .map(response => response.json());
    }
}