import forEach = require("core-js/fn/array/for-each");

export class LGQuestion{
    description: string;
    questionType: boolean;
    title: string;
    answersId: Array<string>;
    questionId: string;

    constructor(){
        this.description = '';
        this.questionType = false;
        this.title = '';
        this.questionId = '';
        this.answersId = [];
    }
}

