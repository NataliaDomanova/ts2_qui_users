import {LGQuestion} from "./question";

export class ReadingTask{
    textTitle: string;
    text: string;
    questions: Array<LGQuestion>;
    
    constructor(){
        this.textTitle = '';
        this.text = '';
        this.questions = [];            
    }
}
