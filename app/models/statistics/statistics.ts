// export class Test {
//     part1:Array<number>;
//     part2:Array<number>;
//     part3:Array<number>;
//     part4:Array<number>;
//     marks:Array<Object>;
//     isChecked:boolean;
//
//     constructor(obj){
//         console.log("obj", obj);
//         this.isChecked = true; //obj.isChecked
//         this.part1 = obj.lexicalGrammar.map(answer => answer.isCorrect) || [0];
//         console.log("part1", this.part1);
//         this.part2 = obj.reading.map(answer => answer.isCorrect) || [0];
//         console.log("part2", this.part2);
//         this.part3 = obj.listening.map(answer => answer.isCorrect) || [0];
//         console.log("part3", this.part3);
//         this.part4 = obj.speaking.map(answer => answer.isCorrect) || [0];
//         console.log("part4", this.part4);
//         this.marks = [
//             {
//                 'value':obj.lexicalGrammar[obj.lexicalGrammar.length - 1],
//                 'length': obj.lexicalGrammar.length - 2,
//                 //'name': 'LGT'
//             },
//             {
//                 'value':obj.reading[obj.reading.length - 1],
//                 'length': obj.lexicalGrammar.length - 2,
//                 'name': 'Reading'
//             },
//             {
//                 'value':obj.listening[obj.listening.length - 1],
//                 'length': obj.lexicalGrammar.length - 2,
//                 'name': 'Listening'
//             },
//             {
//                 'value':obj.speaking[obj.speaking.length - 1],
//                 'length': obj.lexicalGrammar.length - 2,
//                 'name': 'Speaking'
//             }
//         ];
//         console.log("marks", this.marks);
//     }
// }

export class Statistics {
    part1:Array<number>;
    part2:Array<number>;
    part3:Array<number>;
    part4:Array<number>;
    marks:Array<number>;
    isChecked:boolean;
    conclusion:string;

    constructor(obj){
        console.log("obj", obj);
        this.isChecked = true;
        this.conclusion = obj.conclusion;
        this.part1 = obj.lexicalGrammar.map(answer => answer.isCorrect).slice(0, -1) || [0];
        this.part2 = obj.reading.map(answer => answer.isCorrect).slice(0, -1) || [0];
        this.part3 = obj.listening.map(answer => answer.isCorrect).slice(0, -1) || [0];
        this.part4 = new Array(this.part1.length);
        var arr = obj.speaking.map(answer => answer.isCorrect).slice(0, -1) || [0];
        for(var i = 0; i < this.part1.length; ++i){
            this.part4[i] = 0;
        }
        for(var i = 0; i < arr.length; ++i){
            this.part4[i] = arr[i];
        }
        this.marks = [obj.lexicalGrammar.slice(-1), obj.reading.slice(-1), obj.listening.slice(-1), obj.speaking.slice(-1)];
    }
}