import {Injectable, OnInit} from '@angular/core';

import {Http, Headers, RequestOptions} from '@angular/http';
import { Observable }     from 'rxjs/Observable'
import {Statistics} from "./statistics";
import 'rxjs/add/operator/toPromise';
import {Constants} from "../../Constants";

@Injectable()
export class StatisticsService{

    constructor(private http: Http) {
    }


    getTestsList(userId:string): Observable<Statistics[]>{
        let authToken = localStorage.getItem('auth_token');
        let headers = new Headers({'Authorization' : authToken});
        let options = new RequestOptions({ headers: headers });
        var url = Constants.BASE_URL + '/tests/' + userId + '/all';
        console.log('url ', url);
        return this.http.get(url, options)
            .map(response => response.json());

    }

    getTestById(testId: string): Observable<Statistics> {
        let authToken = localStorage.getItem('auth_token');
        let headers = new Headers({'Authorization' : authToken});
        let options = new RequestOptions({ headers: headers });
        var url = Constants.BASE_URL + '/userAnswers/' + testId + '/statistics';
        console.log('url ', url);
        return this.http.get(url, options)
            .map(response => response.json());
    }
}