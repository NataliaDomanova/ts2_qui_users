export class CheckedAnswers {
    answerId: string;
    cost: number;
    description: string;

    constructor(){
        this.answerId = '';
        this.cost = 0;
        this.description = '';
    }
}
