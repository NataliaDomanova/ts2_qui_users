import {CheckedAnswers} from "./checkedAnswers";
export class AnswerForTeacher extends CheckedAnswers {
    answer: string;
    question: string;

    constructor(){
        super();
        this.answer = '';
        this.question = '';
    }
}
