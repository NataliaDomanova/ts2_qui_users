export class Answer {
	answer: string;
	questionId: string;
	isCorrect:boolean;
	
	constructor(){
		this.answer = '';
		this.questionId = '';
	}
}