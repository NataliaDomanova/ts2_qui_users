import {ReadingTask} from "../question/ts-user-test.modelReadingTask";
import {LGQuestion} from "../question/question";

export class TestModel{
    id: string;
    readingTask: ReadingTask;
    listeningTask: ReadingTask;
    lgtTask: Array<LGQuestion>;
    speaking: Array<LGQuestion>;

    constructor(){
        this.id = '';
        this.readingTask = new ReadingTask();
        this.listeningTask = new ReadingTask();
        this.lgtTask = [];
        this.speaking = [];
    }
}


